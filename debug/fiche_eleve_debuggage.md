## Etape 1

Le professeur présente l'exercice. 
7 programmes sont présentés, contenant chacun une erreur différente.
La réalisation du programme 1 peut se faire de façon collective pour que l'élève comprenne bien le travail à effectuer sur les autres programmes.

## Etape 2
L’exercice 1 se fait de façon individuelle, sur Capytale (ou Jupyter)
On observe là où les élèves bloquent en passant dans les rangs.
- Compréhension de l'erreur
- Correction pertinente du programme

Mise en commun collective des réponses oralement avec discussion si la question ou réponse
ambigüe.

Rédaction des significations des exceptions sur une fiche de synthèse.

## Etape 3
L’exercice 2 se fait de façon individuelle. L'élève met en pratique les connaissances découvertes dans l'exercice 1 pour corriger un programme plus complexe, présentant plusieurs erreurs de nature différente.

On observe là où les élèves bloquent en passant dans les rangs.
- Compréhension de l'erreur
- Correction pertinente du programme

Mise en commun collective des réponses oralement avec discussion si la question ou réponse
ambigüe.
