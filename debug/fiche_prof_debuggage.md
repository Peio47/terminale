**Thématique :** Programmation

**Notions liées :** dictionnaire, POO

**Résumé de l’activité :** Activité sur ordinateur en deux parties. Analyse des informations de debuggage et correction des programmes.

**Objectifs :** Recensement des exceptions les plus courantes. Analyse de ces exceptions. Correction de programme contenant des bugs. 

**Auteur :** Peio

**Durée de l’activité :** 2h 

**Forme de participation :** individuelle ou en binôme, en autonomie, collective 

**Matériel nécessaire :** ordinateur

**Préparation :** Aucune

**Autres références :** 

**Fiche élève cours :** 

**Fiche élève activité :** Disponible [ici](https://gitlab.com/sebhoa/mooc-2-ressources/-/blob/main/1_Penser-Concevoir-Elaborer/fiche_eleve_decouverte_BDR_charles.pdf)   
