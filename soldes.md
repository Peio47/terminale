# Exercice 1 : Courses de rentrée

<div class="alert alert-info" style="border-left:15px solid #31708f;">
    <b>Objectif : </b> Ecrire la définition d’une classe. <br />
</div>


On a créé dans cet exercice une classe ``Article`` ayant trois attributs :

* `description` de type `str`,

* `prix` de type `float`,

* `remise` de type `int`, exprimée en pourcentage.

Cette classe possède aussi une méthode décrites ci-dessous (`article` est un objet de type `Article`) :

* `article.prix_apres_reduction()` qui renvoie la valeur du prix auquel on a appliqué une réduction. La réduction n'est appliquée que dans la mesure où le montant de cette réduction, exprimée en euros, est supérieure à 1€.


```mermaid
classDiagram
class Article{
       description : str
       prix : float
       reduction : int
       prix_apres_reduction() float
}
```

Le code de cette classe est donné ci-dessous (*ne pas oublier de l'exécuter*):
```python
class Article:
    def __init__(self, description, prix):
        '''Constructeur'''
        self.description = description
        self.prix = prix
        self.remise = 0

    def prix_apres_reduction(self) -> float:
        '''Renvoie le prix de l'article après réduction'''
        reduction = self.prix*self.remise/100
        if reduction < 1:
            nouveau_prix = self.prix
        else:
            nouveau_prix = self.prix-reduction
        return round(nouveau_prix, 2)
```

En utilisant cette classe ``Article``, **créer** un tableau ``courses`` contenant les articles suivants :
* un cartable à 45 €
* une boite de crayons de couleur à 9.95 €
* une trousse à 12 €

**Créer** une fonction ``somme_articles_reduction`` qui prend en attributs une liste d'articles et une remise exprimée en pourcentage par un entier inférieur à 100.

La somme sera arrondie au centime près.
