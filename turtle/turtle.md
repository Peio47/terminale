# Les polygones réguliers

Le but de cette activité est de dessiner des polygones réguliers grâce au module turtle.

Adapté de la fameuse Tortue de Logo, un langage informatique en partie destiné à apprendre en créant, le module turtle de Python offre un vaste espace de jeu avec l’utilisation simple et élargie d’une interface graphique !

On trouve un aide-mémoire présentant toutes les fonctionnalités du module [ici](https://capytale2.ac-paris.fr/web/sites/default/files/2022/02-15/12-21-39/image.png)

Un polygone régulier est un Polygone à la fois équilatéral (tous ses côtés ont la même longueur) et équiangle (tous ses angles ont la même mesure).

L'exemple suivant permet de tracer un carré (polygone régulier à 4 côtés) :
```python
from turtle import*

for i in range(4):
    forward(100)
    right(90)
```

Q1) En vous aidant du manuel de base et de l'aide mémoire, **créer** la procédure ``triangle_equi()`` qui trace un triangle équilatérale en passant la longueur de ses côtés en paramètre.

On souhaite maintenant construire un polygone régulier quel que soit le nombre de ses côtés.

On suivra l'algorithme suivant, décrit en langage naturel :

```
triangle_equi(cote)
    Répéter 3 fois
        avancer(cote)
        tourner_gauche(120)
```
Q2) **Créer** une fonction ``angle_poly()`` qui renvoie l'angle de construction en fonction du nombre de côtés du polygone 

*Remarque :* la somme totale des angles de construction est égale à 360°

On suivra l'algorithme suivant, décrit en langage naturel :

```
angle_poly(nbre)
    angle = 360 / nbre
    renvoyer angle
```

Q3) **Créer** une procédure ``poly_reg()`` qui construit un polygone régulier en prenant comme paramètres la longueur et le nombre de côtés.

On suivra l'algorithme suivant, décrit en langage naturel :

```
poly_reg(cote, nbre)
    angle = angle_poly(nbre)
    Répéter nbre fois
        avancer(cote)
        tourner_gauche(angle)
```
Q4) **Modifier** la procédure ``poly_reg()`` pour tracer en bleu les polygones dont le nombre de côtés est pair, et en rouge les autres.


```
poly_reg(cote, nbre)
    angle = angle_poly(nbre)
    Si nbre est pair  Alors
        couleur('blue')
    Sinon
        couleur('red')
    Répéter nbre fois
        avancer(cote)
        tourner_gauche(angle)
```

*Rappel :* l'opérateur % renvoie le reste de la division de deux nombres.

Q5) **Créer** la fonction ``poly_alea()`` un polygone régulier avec un nombre aléatoire de côtés. La fonction prend comme paramètre la longueur des côtés et renvoie True si le polygone créé est un triangle.

```
poly_alea(cote)
    nbre_alea = entier aléatoire entre 1 et 10
    poly_reg(100, angle_alea)
    Si nbre_alea égale 3  Alors
        renvoyer Vrai
    Sinon 
        renvoyer Faux
```

*Rappel :* la fonction ``randint(a,b)`` du module random renvoie un entier aléatoire compris entre les nombres ``a`` et ``b``.

Q6) **Ecrire** un programme qui construit des polygones aléatoires jusqu'à construire un triangle.


```
est_Triangle = Faux
Tant est_Triangle n'est pas Vrai Faire
    est_Triangle = poly_alea(100)
```
