﻿import pile as p

def creer_file_vide():
    return (p.creer_pile_vide(), p.creer_pile_vide())

def est_vide(file):
    return p.est_vide(file[0]) and p.est_vide(file[1])

def enfiler(file, valeur):
    p.empiler(file[0], valeur)

def defiler(file):
    if p.est_vide(file[1]):
        while not p.est_vide(file[0]):
            valeur = p.depiler(file[0])
            p.empiler(file[1], valeur)
    return p.depiler(file[1])