On a créé dans cet exercice une classe ``Pile`` ayant trois attributs :

* `_sommet` de type `int`,

* `_reste` de type `list`,

* `_longueur` de type `int`, exprimée en pourcentage.

Cette classe possède aussi une méthode décrites ci-dessous :

* `empile(valeur : int)` qui renvoie la valeur du prix auquel on a appliqué une réduction. La réduction n'est appliquée que dans la mesure où le montant de cette réduction, exprimée en euros, est supérieure à 1€.

* `empile(valeur : int)` qui renvoie la valeur du prix auquel on a appliqué une réduction. La réduction n'est appliquée que dans la mesure où le montant de cette réduction, exprimée en euros, est supérieure à 1€.


```mermaid
classDiagram
class Pile{
       _sommet : int
       _reste : list
       _longueur : int
       empile(valeur : int) 
       depile() int
       est_vide() bool
       __repr__() str
}
```
