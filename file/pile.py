﻿'''Le module pile permet une implémentation d'un structure de pile basée sur les opérations suivantes :
    pile_vide : renvoie une pile vide
    empiler : empile une valeur donnée dans la pile_vide
    depiler : dépile et renvoie une valeur de la pile_vide
    est_vide : indique si la pile est vide
'''

def creer_pile_vide():
    '''
    objectif : créer une pile vide
    paramètres : Aucun
    retour : pile vide
    '''
    return []

def empiler(pile, valeur):
    '''
    objectif : empiler une valeur dans la pile
    paramètres : une pile, une valeur (peu importele type)
    retour : Rien
    '''
    pile.append(valeur)


def depiler(pile):
    '''
    objectif : dépiler une valeur de la pile
    paramètres : une pile
    retour : l'élément dépilé
    '''
    return pile.pop()

def est_vide(pile):
    '''
    objectif : détermine si une pile vide
    paramètres : une pile
    retour : booléen, True si la pile est vide, False sinon
    '''
    return len(pile)==0