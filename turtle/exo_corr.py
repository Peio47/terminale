from turtle import*
from random import randint

speed(0)

def triangle_equi(long):
    for i in range(3):
        forward(long)
        left(120)


def angle_poly(cote):
    return 360/cote

def poly_reg(cote, long) :
    angle = angle_poly(cote)
    if cote%2 == 0:
        color('blue')
    else :
        color('red')
    for i in range(cote):
        forward(long)
        left(angle)

def poly_alea():
    cote = randint(3,10)
    poly_reg(cote,100)
    return cote

nb_cote = 0
while nb_cote !=3:
    nb_cote = poly_alea()
