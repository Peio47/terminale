# Implémentation par des piles (Métropole 2021)
<div class="alert alert-info" style="border-left:15px solid #31708f;">
    <b>Objectif : </b> Écrire plusieurs implémentations d’une même structure de données. <br />
</div>

On choisit d'implémenter une `file` à l'aide d'un couple ```(p1,p2)``` où `p1` et `p2` sont des piles. Ainsi `file[0]` et `file[1]` sont respectivement les piles `p1` et `p2`.

Pour enfiler un nouvel élément `valeur` dans `file`, on l'empile dans `p1`.

Pour défiler `file`, deux cas se présentent :
* La pile `p2` n'est pas vide : on dépile `p2`.
* La pile `p2` est vide : on dépile les éléments de `p1` en les empilant dans `p2` jusqu'à ce que `p1` soit vide, puis on dépile `p2`.
![image.png](image.png)

Le module `pile` attaché à cette page donne une implémentation de cette structure de données.

**Consulter** la documentation de ce module.

A partir des opérations sur les piles fournis dans le module, **écrire** en Python les fonctions suivantes :
  * une fonction ``creer_file_vide()`` qui renvoie une ``file`` vide de tout élément.
  * une fonction ``est_vide(file)`` qui prend en argument une ``file`` et qui renvoie ``True`` si la ``file`` est vide, ``False `` sinon.
  * une fonction ``enfiler(file, valeur)`` qui prend en arguments une ``file`` et un élément ``valeur`` et qui ajoute ``valeur`` en queue de la `file`.
  * une fonction ``defiler(file)`` qui prend en argument une ``file`` et qui renvoie l'élément en tête de la ``file`` en le retirant.

