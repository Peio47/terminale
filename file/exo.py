﻿import pile as p

def creer_file_vide():
    ...

def est_vide(file):
    ...

def enfiler(file, valeur):
    ...

def defiler(file):
    ...


ma_file = creer_file_vide()
enfiler(ma_file, 1)
enfiler(ma_file, 2)
enfiler(ma_file, 3)
enfiler(ma_file, 4)
enfiler(ma_file, 5)
assert defiler(ma_file) == ...
assert defiler(ma_file) == ...